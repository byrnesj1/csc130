public class LinkList {
	public Link tail;
	public Link first;

	public LinkList()
	{
		tail = null;
		first = null;
	}


	public boolean isEmpty() {
		return first == null;
	}


	//push item to end of list
	public void insert(int d1, double d2) {
		Link newest = new Link(d1,d2);
		newest.next = null;
		
		if (first == null) //case when list is empty
		{
			first = newest;
			tail = newest;
		}

		else
		{
			tail.next = newest; //current end points to the newest node
			tail = newest; //the end node is now the newest node
		}
	}



}