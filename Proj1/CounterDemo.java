
	public static void main(String[] args){
		Counter c;
		c = new Counter();
		c.increment();
		int first = c.getCount();
		c.increment(100);
		int second = c.getCount();
		c.reset();
		int third = c.getCount();
		System.out.println(first);
		System.out.println(second);
		System.out.println(third);
		}
	