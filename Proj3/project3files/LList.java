//Jeffrey Byrnes
public class LList<DataCount> 
{
	public class Node<DataCount>
	{
		DataCount data;
		private Node<DataCount> next;

		public Node(DataCount e, Node<DataCount> n)
		{
			this.data = e;
			this.next = n;
		}

		public DataCount getElement() {return data;}
		public Node<DataCount> getNext() {return next;}

		public void setNext(Node<DataCount> n) {next = n;}
	}

	public Node tail = null;
	public Node root = null;
	public int size = 0;

	public LList() {}

	public LList(DataCount entry)
	{
		Node first = new Node(entry,null);
		root = first;
	}

	public boolean isEmpty()
	{
		return root == null;
	}

	public void addData(DataCount data)
	{
		Node newNode = new Node(data,null);
		
		if (isEmpty())
		{
			root = newNode;
			tail = root;
		}

		else
		{
			tail.setNext(newNode);
		}
		tail = newNode;
		size++;

	}



}