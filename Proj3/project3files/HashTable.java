//Jeffrey Byrnes
import java.util.Arrays;

/**
 * TODO Replace this comment with your own.
 * 
 * Stub code for an implementation of a DataCounter that uses a hash table as
 * its backing data structure. We included this stub so that it's very clear
 * that HashTable works only with Strings, whereas the DataCounter interface is
 * generic.  You need the String contents to write your hashcode code.
 */
public class HashTable implements DataCounter<String> {

    DataCount<String>[] hash = new DataCount[50000];
    int uniqueStrings = 0;
    int size = 0;

    public HashTable()
    {
        DataCount<String>[] hash = new DataCount[50000];
        int uniqueStrings = 0;
        int size = 0;
    }


    public int hashFunction(String data, DataCount<String>[] table)
    {
        int value = 0;

        for (int i = 0; i < data.length(); i++)
        {
            value += (int)data.charAt(i);
        }

        return (value % table.length);
    }



    
    public void mapToHash(DataCount<String>[] table, String data)
    {
        data = data.toLowerCase();
        int value = hashFunction(data,table);
        DataCount<String> entry = new DataCount(data,1);

        int i = 1;
        while (table[value] != null)
        {
            if (table[value].data.equals(data))
            {
                ++table[value].count;
                break;
            }

            else
            {
                value = (value + i*i)%table.length;
                ++i;
            }
        }

        if (table[value] == null)
        {
            table[value] = entry;
            ++uniqueStrings;
        }

    }



    /** {@inheritDoc} */
    public DataCount<String>[] getCounts() {
        // TODO Auto-generated method stub
        DataCount<String> temp;
        int flag = 0;


 

        /*
        for (int i = 0; i < hash.length; i++)
        {
            if (hash[i] != null)
            {
            temp = hash[i];
                for (int j = i+1; j < hash.length; j++)
                {
                    if (hash[j] != null)
                    {
                        if (hash[j].getElement().compareTo(temp.getElement()) < 0)
                        {
                            temp = hash[j];
                            flag = j;
                        }
                    }

                }
                hash[flag] = hash[i];
                hash[i] = temp;
            }
        }*/
    



        return hash;
    }


    /** {@inheritDoc} */
    public int getSize() {
        // TODO Auto-generated method stub
        return size;
    }

    /** {@inheritDoc} */

    public void incCount(String data) {
        // TODO Auto-generated method stub
        ++size;
        hash = checkSize(hash);
        mapToHash(hash,data);
        

        /*
        data = data.toLowerCase();
        int value = 0;
        value = hashFunction(data,hash);
      


        if (hash[value] == null)
        {
            DataCount<String> entry = new DataCount(data,1);
            hash[value] = entry;
            ++uniqueStrings;
            return;

        }

        else if (hash[value].data.equals(data))
        {
            ++hash[value].count;
            //System.out.println(hash[value].data.compareTo(data));

            //System.out.println(hash[value].data.getClass().getSimpleName() + " " + data.getClass().getSimpleName());
            
            //System.out.println(hash[value].data + ": " + hash[value].count);
            return;
        }

        
        else if (hash[value] != null && !hash[value].data.equals(data))
        {
            //System.out.println(hash[value].data + " " + data);
            int i = 1;
            
            while (true)
            {
                value = (value + (i*i)) %hash.length;
                
                if (hash[value] == null)
                {
                    ++uniqueStrings;
                    DataCount<String> entry = new DataCount(data,1);
                    hash[value] = entry;
                    break;
                }
                else
                {
                    if (hash[value].data.equals(data))
                    {
                        hash[value].count = hash[value].count+1;
                        //System.out.println(hash[value].data + ": " + hash[value].count);
                    }
                }
            ++i;
            value = (value + (i*i)) % hash.length;
            }


        }*/



        //table[value] = entry;

    }

    public DataCount<String>[] checkSize(DataCount<String>[] hash)
    {
        double size = (double)hash.length;
        double entries = (double)uniqueStrings;



        if (entries / size >= 0.75)
        {
            hash = newHash(hash,(2*hash.length+1));
        }

        return hash;
    }

    public DataCount<String>[] newHash(DataCount<String>[] hash, int size)
    {

        DataCount<String>[] newTable = new DataCount[size];
        int value = 0;
        int j;

        for (int i = 0; i < hash.length; i++)
        {

            if (hash[i] != null)
            {
                j = 1;

                value = hashFunction(hash[i].getElement(),newTable);

                if (newTable[value] == null)
                {
                    newTable[value] = hash[i];
                }

                
                else
                {
                    value = (value + (j*j)) % size;
                    while (newTable[value] != null)
                    {

                        value = (value + (j*j)) % size;
                        j = j + 1;
                    }

                    newTable[value] = hash[i];
                }

            }
        }

        return newTable;




    }


}
