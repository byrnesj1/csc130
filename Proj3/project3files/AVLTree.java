//Jeffrey Byrnes

public class AVLTree<E extends Comparable<? super E>> extends BinarySearchTree<E>
{

	//null constructor
	public AVLTree() {
		overallRoot = null;
	}

	BinarySearchTree<String>.BSTNode overallRoot;
	int size = 0;
	int uniqueNodes = 0;
	DataCount<String>[] counts = new DataCount[uniqueNodes];

	private int height(BinarySearchTree<String>.BSTNode node)
	{
		return node == null ? -1 : node.height;
	}

	public BinarySearchTree<String>.BSTNode incCount(String data, BinarySearchTree<String>.BSTNode node)
	{

		if (overallRoot == null)
		{
			overallRoot = new BinarySearchTree<String>.BSTNode(data);
			++uniqueNodes;
			++size;
		}

		else
		{
			if (node == null)
			{
				node = new BinarySearchTree<String>.BSTNode(data);
				++uniqueNodes;
				++size;
			}

			else
			{
				int z = data.compareTo(node.data);
				if (z == 0)
				{
					++node.count;
					++size;
				}

				else if (z < 0)
				{
					node.left = incCount(data,node.left);
					if (height(node.left) - height(node.right) == 2)
					{
						if (data.compareTo(node.left.data) < 0)
							{node = rotateWithLeftChild(node);}
						else
							{node = doubleWithLeftChild(node);}
					}
				}
				else if (z > 0)
				{
					node.right = incCount(data,node.right);
					if (height(node.right) - height(node.left) == 2)
					{
						if (data.compareTo(node.right.data) > 0)
							{node = rotateWithRightChild(node);}
						else
							{node = doubleWithRightChild(node);}
					}
				}

				node.height = Math.max(height(node.left),height(node.right)) + 1;
			}
		}
	return node;
	}

	private BinarySearchTree<String>.BSTNode rotateWithRightChild(BinarySearchTree<String>.BSTNode k1)
	{

		BinarySearchTree<String>.BSTNode k2 = k1.right;

		if (k1.data.compareTo(overallRoot.data) == 0)
		{
			overallRoot = k2;
		}


		k1.right = k2.left;
		k2.left = k1;

		k1.height = Math.max(height(k1.left),height(k1.right))+1;
		k2.height = Math.max(height(k2.right),k1.height) + 1;
		return k2;
	}

	private BinarySearchTree<String>.BSTNode rotateWithLeftChild(BinarySearchTree<String>.BSTNode k2)
	{
		BinarySearchTree<String>.BSTNode k1 = k2.left;

		if (k2.data.compareTo(overallRoot.data) == 0)
		{
			overallRoot = k1;
		}

		k2.left = k1.right;
		k1.right = k2;
		k2.height = Math.max(height(k2.left),height(k2.right))+1;
		k1.height = Math.max(height(k1.left),k2.height) +1;
		return k1;
	}

	private BinarySearchTree<String>.BSTNode doubleWithLeftChild(BinarySearchTree<String>.BSTNode k3)
	{
		k3.left = rotateWithRightChild(k3.left);
		return rotateWithLeftChild(k3);
	}

	private BinarySearchTree<String>.BSTNode doubleWithRightChild(BinarySearchTree<String>.BSTNode k1)
	{
		k1.right = rotateWithLeftChild(k1.right);
		return rotateWithRightChild(k1);
	}
	






	/*
	public DataCount[] getCounts()
	{
		DataCount[] counts = new DataCount[uniqueNodes];
		//BinarySearchTree.BSTNode root = new BinarySearchTree.BSTNode(overallRoot.data,overallRoot.count,overallRoot.left,overallRoot.right,overallRoot.size,overallRoot.height);
		int w = traverse(overallRoot,counts,0);

		return counts;
	}*/

	/*public Iterable<BinarySearchTree<String>.BSTNode> children(BinarySearchTree<String>.BSTNode node)
	{
		ArrayList<BinarySearchTree<String>.BSTNode> childs = new ArrayList<>(2);
		if (node.left != null)
		{
			childs.add(node.left);
		}

		if (node.right != null)
		{
			childs.add(node.right);
		}
		return childs;
	}*/



	/*
	@Override
	public int traverse(BinarySearchTree<String>.BSTNode node, DataCount<String>[] counts, int idx)
	{
		if (node != null)
		{
			idx = traverse(node.left,counts,idx);
			counts[idx] = new DataCount(node.data,node.count);
			idx = traverse(node.right,counts,idx+1);
		}
		return idx;
	}

	@Override
 	public DataCount<E>[] getCounts()
 	{
 		DataCount<E>[] counts = new DataCount[uniqueNodes];
 		if (overallRoot != null)
 		{
 			traverse(overallRoot, counts,0);
 		}
 		return counts;
 	}*/




























	/*
	public BinarySearchTree<String>.BSTNode insert(String data, BinarySearchTree<String>.BSTNode node)
	{
		if (node == null)
		{
			node = new BinarySearchTree<String>.BSTNode(data);			
		}

		else if (data.compareTo(node.data) < 0)
		{
			node.left = insert(data,node.left);
			
			if ((height(node.left) - height(node.right)) > 1)
			{
				if (data.compareTo(node.left.data) < 0)
				{
					node = SingleRight(node);
				}
				else
				{
					node = DoubleLeftRight(node);
				}
			}
		}

		else if (data.compareTo(node.data) >= 0)
		{

			node.right = insert(data,node.right);

			if ((height(node.right) - height(node.left)) > 1)
			{
				if (data.compareTo(node.right.data) >= 0)
				{
					node = SingleLeft(node);
				}
				else
				{
					node = DoubleRightLeft(node);
				}
			}

		}
	node.height = Math.max( height( node.left ), height( node.right ) ) + 1;
	return node;
	}

	public BinarySearchTree<E>.BSTNode SingleRight(BinarySearchTree<E>.BSTNode node)
	{
		BinarySearchTree<E>.BSTNode temp = node.left;
		
		if (temp.right != null)
			{node.left = temp.right;}

		else {node.left = null;}

		temp.right = node;

		if (node.data == overallRoot.data)
		{
			overallRoot = temp;
		}

		 node.height = Math.max( height( node.left ), height( node.right ) ) + 1;
         temp.height = Mathm.nax( height( temp.left ), node.height ) + 1;

		return temp;
	}

	public BinarySearchTree<E>.BSTNode SingleLeft(BinarySearchTree<E>.BSTNode node)
	{
		BinarySearchTree<E>.BSTNode temp = node.right;
		if (temp.left != null)
			{node.right = temp.left;}

		else {node.left = null;}

		temp.left = node;

		if (node.data == overallRoot.data)
		{
			overallRoot = temp;
		}

		 node.height = Math.max( height( node.left ), height( node.right ) ) + 1;
         temp.height = Math.max( height( temp.right ), node.height ) + 1;

		return temp;
	}

	public BinarySearchTree<E>.BSTNode DoubleRightLeft(BinarySearchTree<E>.BSTNode node)
	{
		node.right = SingleRight(node.right);
		return SingleLeft(node);
	}

	public BinarySearchTree<E>.BSTNode DoubleLeftRight(BinarySearchTree<E>.BSTNode node)
	{
		node.left = SingleLeft(node.left);
		return SingleRight(node);
	}

	/*
	//check for unbalances
	public BinarySearchTree<E>.BSTNode NodeCheck(BinarySearchTree<E>.BSTNode node)
	{
		int leftHeight = node.leftHeight();
		int rightHeight = node.rightHeight();


		int z = Math.abs(leftHeight - rightHeight);

		if (z > 1)
		{
			System.out.println();
			System.out.println("PROBLEM DETECTED AT NODE " + node.data);
			return node;
		}

		for (final BinarySearchTree<E>.BSTNode c : children(node))
		{
			BSTNode res = NodeCheck(c);
			if (findHeight(c) > 0 && z <= 1)
			{
				res = NodeCheck(c);
				return res;
			}
		}
	return null;
	}





	//apply approiate rotation
	public void fixProblem(BinarySearchTree<E>.BSTNode node)
	{
		int leftHeight = node.leftHeight();
		int rightHeight = node.rightHeight();

		if (leftHeight > rightHeight)
		{
			int llHeight = node.left.leftHeight();
			int lrHeight = node.left.rightHeight();

			if (llHeight > lrHeight)
			{
				SingleRight(node);
			}

			else
			{
				DoubleLeftRight(node);
			}
		}

		else
		{
			int rlHeight = node.right.leftHeight();
			int rrHeight = node.right.rightHeight();

			if (rrHeight > rlHeight)
			{
				SingleLeft(node);
			}

			else
			{
				DoubleRightLeft(node);
			}
		}
	}

	//rotations
	public void SingleRight(BinarySearchTree<E>.BSTNode node)
	{
		BinarySearchTree<E>.BSTNode temp = node.left;
		node.left = temp.right;
		temp.right = node;


		if (node.data == overallRoot.data)
		{
			overallRoot = temp;
		}

	//	node = temp;
	}

	public void SingleLeft(BinarySearchTree<E>.BSTNode node)
	{
		BinarySearchTree<E>.BSTNode temp = node.right;
		node.right = temp.left;
		temp.left = node;
		

		if (node.data == overallRoot.data)
		{
			overallRoot = temp;
		}

	//	node = temp;
	}

	public void DoubleLeftRight(BinarySearchTree<E>.BSTNode node)
	{
		SingleLeft(node.left);
		SingleRight(node);
	}

	public void DoubleRightLeft(BinarySearchTree<E>.BSTNode node)
	{
		SingleRight(node.right);
		SingleLeft(node);
	}

	//tree balancer
	public void balance()
	{
		BinarySearchTree<E>.BSTNode problem = NodeCheck(overallRoot);

		while (problem != null)
		{
			fixProblem(problem);
			problem = NodeCheck(overallRoot);
		}
	}
	*/

}