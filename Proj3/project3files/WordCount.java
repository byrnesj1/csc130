//Jeffrey Byrnes


import java.io.IOException;
import java.util.Scanner;

/**
 * An executable that counts the words in a files and prints out the counts in
 * descending order. You will need to modify this file.
 */
public class WordCount {

    private static void countWords(String file, char type, String metric) {
        if (type == 'b')
        {

            BinarySearchTree<String> bst = new BinarySearchTree();

            try {
                FileWordReader reader = new FileWordReader(file);
                String word = reader.nextWord();
                while (word != null) {
                    bst.incCount(word);
                    word = reader.nextWord();
                }
            }
            catch (IOException e) {
                System.err.println("Error processing " + file + e);
                System.exit(1);
            }

            DataCount<String>[] nCounts = bst.getCounts();
            DataCount<String>[] counts = new DataCount[bst.uniqueNodes];

            int n = 0;
            for (int i = 0; i < nCounts.length; i++)
            {
                if (nCounts[i] != null)
                {
                    counts[n] = nCounts[i];
                    n++;
                }
            }
            sortByDescendingCount(counts);
            
            if (metric.equals("-frequency"))
            {   
            for (int i = 0; i < counts.length; i++)
                System.out.println(counts[i].count + " \t" + counts[i].data);
            }

            else if (metric.equals("-num_unique"))
            {
                System.out.println("Number of unique words: " + bst.uniqueNodes);
            }
        }

        else if (type == 'a')
        {
            AVLTree<String> avl = new AVLTree();

            try {
                FileWordReader reader = new FileWordReader(file);
                String word = reader.nextWord();

                while (word != null)
                {
                    avl.incCount(word, avl.overallRoot);
                    word = reader.nextWord();
                }} catch (IOException e) {
                    System.err.println("Error processing " + file + e);
                    System.exit(1);
                }

                DataCount<String>[] counts = new DataCount[avl.uniqueNodes];

                int x1 = avl.traverse(avl.overallRoot,counts,0);
                sortByDescendingCount(counts);

                if (metric.equals("-frequency"))
                {
                for (int i = 0; i < counts.length; i++)
                    System.out.println(counts[i].count + " \t" + counts[i].data);
                }

                else if (metric.equals("-num_unique"))
                {
                    System.out.println("Number of unique words: " + avl.uniqueNodes);
                }

            }

        else if (type == 'h')
        {
            HashTable hash = new HashTable();

            try {
                FileWordReader reader = new FileWordReader(file);
                String word = reader.nextWord();
            

            while (word != null)
            {
                hash.incCount(word);
                word = reader.nextWord();
            }} catch(IOException e) {
                System.err.println("Error processing " + file + e);
                System.exit(1);
            }

            DataCount<String>[] nCounts = hash.getCounts();
            DataCount<String>[] counts = new DataCount[hash.uniqueStrings];

            int n = 0;

            for (int i = 0; i < nCounts.length; i++)
            {
                if (nCounts[i] != null)
                {
                    counts[n] = nCounts[i];
                    n++;
                }
            }

            sortByDescendingCount(counts);
            
            if (metric.equals("-frequency"))
            {
            for (int i = 0; i < counts.length; i++)
                System.out.println(counts[i].count + " \t" + counts[i].data);
            }
            else if (metric.equals("-num_unique"))
            {
                System.out.println("Number of unique words: " + hash.uniqueStrings);
            }

        
        }
    }

    /**
     * TODO Replace this comment with your own.
     * 
     * Sort the count array in descending order of count. If two elements have
     * the same count, they should be in alphabetical order (for Strings, that
     * is. In general, use the compareTo method for the DataCount.data field).
     * 
     * This code uses insertion sort. You should modify it to use a different
     * sorting algorithm. NOTE: the current code assumes the array starts in
     * alphabetical order! You'll need to make your code deal with unsorted
     * arrays.
     * 
     * The generic parameter syntax here is new, but it just defines E as a
     * generic parameter for this method, and constrains E to be Comparable. You
     * shouldn't have to change it.
     * 
     * @param counts array to be sorted.
     */
    private static <E extends Comparable<? super E>> void sortByDescendingCount(
            DataCount<String>[] counts) {
        for (int i = 1; i < counts.length; i++) {
            DataCount<String> x = counts[i];
            int j;
            for (j = i - 1; j >= 0; j--) {
                if (counts[j].count >= x.count) {
                    break;
                }
                counts[j + 1] = counts[j];
            }
            counts[j + 1] = x;
        }
    }

    public static void main(String[] args) {
        /*if (args.length != 1) {
            System.err.println("Usage: filename of document to analyze");
            System.exit(1);
        }
        countWords(args[0]);*/

        String type = args[0];
        String metric = args[1];
        String file = args[2];
        char c;
        Scanner sc = new Scanner(System.in);

        if (type.length() < 2)
        {
            c = 'x';
        }
        
        else 
        {
            c = type.charAt(1);
        }

        while (c != 'a' && c != 'b' && c != 'h')
        {
            System.out.println("Re-enter structure type (-a -b -h): ");
            type = sc.nextLine();
            if (type.length() > 1)
            {
                c = type.charAt(1);
            }
        }

        while (!metric.equals("-frequency") && !metric.equals("-num_unique"))
        {
            System.out.println("Re-enter metric desired (-frequency -num_unique)");
            metric = sc.nextLine();
        }


        countWords(file,c,metric);




    }
}
