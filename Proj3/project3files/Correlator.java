//Jeffrey Byrnes

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;


public class Correlator
{
	public static void main(String[] args) throws FileNotFoundException, IOException
	{
		String type = args[0];
		String file1 = args[1];
		String file2 = args[2];


		char c;

		if (type.length() < 2)
		{
			c = 'x';
		}

		else
		{
			x = type.charAt(1);
		}



		Scanner sc = new Scanner(System.in);

		while (c != 'a' && c != 'b' && c != 'h')
		{
			System.out.println("Please re-enter flag for data structure (-a,-b,-h): ");
			type = sc.nextLine();
			if (type.length() > 1)
			{
			c = type.charAt(1);
			}


		}		

		double diff = 0;
		double f1;
		double f2;



		



		FileWordReader fRead1 = new FileWordReader(file1);
		FileWordReader fRead2 = new FileWordReader(file2);

		String w1 = fRead1.nextWord();
		String w2 = fRead2.nextWord();


		if (c == 'a')
		{


			AVLTree<String> avl1 = new AVLTree();
			AVLTree<String> avl2 = new AVLTree();


			while (w1 != null)
			{
				avl1.incCount(w1, avl1.overallRoot);
				w1 = fRead1.nextWord();
			}

			while (w2 != null)
			{
				avl2.incCount(w2, avl2.overallRoot);
				w2 = fRead2.nextWord();
			}

			DataCount<String>[] counts1 = new DataCount[avl1.uniqueNodes];
			DataCount<String>[] counts2 = new DataCount[avl2.uniqueNodes];

			int x1 = avl1.traverse(avl1.overallRoot,counts1,0);
			int x2 = avl2.traverse(avl2.overallRoot,counts2,0);



			int flag = 0;
			for (int i = 0; i < avl1.uniqueNodes; i++)
			{
				f1 = (double)counts1[i].count / (double)avl1.size;
				for (int j = 0; j < avl2.uniqueNodes; j++)
				{
					flag  = 0;
					if (counts1[i].data.equals(counts2[j].data))
					{
						flag = 1;
						f2 = (double)counts2[j].count/(double)avl2.size;
						diff += (f1-f2)*(f1-f2);
						counts2[j].count = 0;
						break;
					}
				}
				if (flag == 0)
				{
					diff += f1*f1;
				}

			}



			for (int i = 0; i < avl2.uniqueNodes; i++)
			{
				f2 = (double)counts2[i].count/(double)avl2.size;
				diff += f2*f2;
			}

			System.out.println(diff);
		}


		else if (c == 'b')
		
		{

			BinarySearchTree<String> bst1 = new BinarySearchTree();
			BinarySearchTree<String> bst2 = new BinarySearchTree();


			while (w1 != null)
			{
				bst1.incCount(w1);
				w1 = fRead1.nextWord();
			}


			while (w2 != null)
			{
				bst2.incCount(w2);
				w2 = fRead2.nextWord();
			}

			DataCount<String>[] counts1 = bst1.getCounts();
			DataCount<String>[] counts2 = bst2.getCounts();


			int flag = 0;
			for (int i = 0; i < bst1.uniqueNodes; i++)
			{
				f1 = (double)counts1[i].count / (double)bst1.getSize();
				for (int j = 0; j < bst2.uniqueNodes; j++)
				{
					flag  = 0;
					if (counts1[i].data.equals(counts2[j].data))
					{
						flag = 1;
						f2 = (double)counts2[j].count/(double)bst2.getSize();
						diff += (f1-f2)*(f1-f2);
						counts2[j].count = 0;
						break;
					}
				}
				if (flag == 0)
				{
					diff += f1*f1;
				}

			}



			for (int i = 0; i < bst2.uniqueNodes; i++)
			{
				f2 = (double)counts2[i].count/(double)bst2.getSize();
				diff += f2*f2;
			}

			System.out.println(diff);
		}


		if (c == 'h')
		{
			HashTable hash1 = new HashTable();
			HashTable hash2 = new HashTable();


			while (w1 != null)
			{
				hash1.incCount(w1);
				w1 = fRead1.nextWord();
			}


			while (w2 != null)
			{
				hash2.incCount(w2);
				w2 = fRead2.nextWord();
			}


			DataCount<String>[] counts1 = hash1.getCounts();
			DataCount<String>[] counts2 = hash2.getCounts();

			DataCount<String>[] nCounts1 = new DataCount[hash1.uniqueStrings];
			DataCount<String>[] nCounts2 = new DataCount[hash2.uniqueStrings];

			int n = 0;
			for (int i = 0; i < counts1.length; i++)
			{
				if (counts1[i] != null)
				{
					nCounts1[n] = counts1[i];
					n++;
				}
			}

			n = 0;
			for (int i = 0; i < counts2.length; i++)
			{
				if (counts2[i] != null)
				{
					nCounts2[n] = counts2[i];
					n++;
				}
			}

			int flag = 0;

			for (int i = 0; i < nCounts1.length; i++)
			{
				f1 = (double)nCounts1[i].count / (double)hash1.size;

				for (int j = 0; j < nCounts2.length; j++)
				{
					if (nCounts1[i].data.equals(nCounts2[j].data))
					{
						f2 = (double)nCounts2[j].count / (double)hash2.size;
						diff += (f1 - f2) * (f1 -f2);
						flag = 1;
						nCounts2[j].count = 0;
						break;
					}
				}

				if (flag == 0)
				{
					diff += f1*f1;
				}
			}

			
			for (int i = 0; i < nCounts2.length; i++)
			{
				f2 = (double)nCounts2[i].count / (double)hash2.size;
				diff += f2*f2;
			}

			System.out.println(diff);
		}
	
	

	



	}
}