public interface BinaryNode<Integer>
{

	public Integer getElement();
	public BinaryNode<Integer> getParent();
	public BinaryNode<Integer> getLeft();
	public BinaryNode<Integer> getRight();
	
}