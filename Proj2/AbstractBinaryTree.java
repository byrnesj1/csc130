public abstract class AbstractBinaryTree<Integer> implements Tree<Integer>
{
	public boolean isInternal(BinaryNode<Integer> p)
	{
		return numChildren(p) > 0;
	}

	public boolean isExternal(BinaryNode<Integer> p)
	{
		return numChildren(p) == 0;
	}

	public boolean isRoot(BinaryNode<Integer> p)
	{
		return p == root();
	}

	public boolean isEmpty()
	{
		return size() == 0;
	}

	public BinaryNode<Integer> sibling(BinaryNode<Integer> p)
	{
		BinaryNode<Integer> parent = p.getParent();
		if (parent == null) 
			return null;

		if (p == parent.getLeft()) 
			return parent.getRight();

		else
			return parent.getLeft();

	}

	public int numChildren(BinaryNode<Integer> p)
	{
		int count = 0;
		if (p.getLeft() != null)
			count++;
		if (p.getRight() != null)
			count++;
		return count;
	}


}