public interface Tree<Integer>
{
	BinaryNode<Integer> root();
	BinaryNode<Integer> parent(BinaryNode<Integer> p);
	int numChildren(BinaryNode<Integer> p);
	boolean isExternal(BinaryNode<Integer> p);
	boolean isInternal(BinaryNode<Integer> p);
	boolean isRoot(BinaryNode<Integer> p);
	int size();
	

}