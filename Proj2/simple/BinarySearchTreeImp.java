public class BinarySearchTreeImp
{
	public static void main(String[] args)
	{
		BinarySearchTree<Integer> bst = new BinarySearchTree<>();

		BinaryNode<Integer> root = bst.addRoot(12);
		BinaryNode<Integer> c1 = bst.addLeft(13,root);
		BinaryNode<Integer> c6 = bst.addRight(22,c1);
		BinaryNode<Integer> c2 = bst.addLeft(14,c1);
		BinaryNode<Integer> c3 = bst.addRight(11,root);
		BinaryNode<Integer> c4 = bst.addRight(10,c3);
		BinaryNode<Integer> c8 = bst.addLeft(200,c3);
		BinaryNode<Integer> c5 = bst.addRight(9,c4);
		

		TreePrinter tprint = new TreePrinter(bst);
		tprint.print("11");

		int y = root.getData() + 7;

		System.out.println(y);
	}
}
