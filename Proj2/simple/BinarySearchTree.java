import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;


public class BinarySearchTree<Integer> 
{
	//instance variables
	int size = 0;
	BinaryNode<Integer> root = null;

	//null constructor
	BinarySearchTree() {}

	//node creator
	//
	protected BinaryNode<Integer> createNode(int e, BinaryNode<Integer> p, BinaryNode<Integer> l, BinaryNode<Integer> r)
	{
		return new BinaryNode<Integer>(e,p,l,r);
	}

	protected BinaryNode<Integer> createEmpty()
	{
		return new BinaryNode<Integer>();
	}

	//tree methods
	public int numChildren(BinaryNode<Integer> e)
	{
		int n = 0;
		if (e.getLeft() != null)
		{
			n++;
		}
		if (e.getRight() != null)
		{
			n++;
		}

		return n;
	}


	public boolean isEmpty()
	{
		return size == 0;
	}

	public boolean isInternal(BinaryNode<Integer> e)
	{
		return numChildren(e) != 0;
	}

	public boolean isExternal(BinaryNode<Integer> e)
	{
		return numChildren(e) == 0;
	}

	public boolean isRoot(BinaryNode<Integer> e)
	{
		return (e == root);
	}




	//get methods
	public int size()
	{
		return size;
	}

	public BinaryNode<Integer> getRoot()
	{
		return root;
	}

	public BinaryNode<Integer> parent(BinaryNode<Integer> e)
	{
		return e.getParent();
	}

	public BinaryNode<Integer> left(BinaryNode<Integer> e)
	{
		return e.getLeft();
	}

	public BinaryNode<Integer> right(BinaryNode<Integer> e)
	{
		return e.getRight();
	}

	//
	public int getData(BinaryNode<Integer> e)
	{
		return e.getData();
	}

	public BinaryNode<Integer> sibling(BinaryNode<Integer> e)
	{
		BinaryNode<Integer> p = e.getParent();
		if (p == null) 
			{return null;}
		if (e == p.getLeft()) 
			{return p.getRight();}
		else 
			{return p.getLeft();}
	}


	//set methods
	public BinaryNode<Integer> addRoot(int e)  throws IllegalStateException
	{
		if (!isEmpty())
		{
			throw new IllegalStateException("Tree already has root.");
		}

		root = createNode(e,null,null,null);
		size = 1;

		return root;
	}

	public void setRoot(int e, BinaryNode<Integer> p, BinaryNode<Integer> l, BinaryNode<Integer> r) throws IllegalStateException
	{


		root = createNode(e,null,l,r);
		size = size - 1;
	}


	public void emptyRoot()
	{
		root = createEmpty();
	}



	public BinaryNode<Integer> addLeft(int e, BinaryNode<Integer> p) throws IllegalArgumentException
	{

		BinaryNode<Integer> child = createNode(e,p,null,null);
		p.setLeft(child);
		++size;

		return child;


	}

	
	public BinaryNode<Integer> addRight(int e, BinaryNode<Integer> p) throws IllegalArgumentException
	{

		BinaryNode<Integer> child = createNode(e,p,null,null);
		p.setRight(child);
		++size;

		return child;

	}


	//height method

	public Iterable<BinaryNode<Integer>> children(BinaryNode<Integer> p)
	{
		ArrayList<BinaryNode<Integer>> childs = new ArrayList<>(2);
		if (left(p) != null)
		{
			childs.add(left(p));
		}

		if (right(p) != null)
		{
			childs.add(right(p));
		}
	return childs;
	}

	public int findHeight(BinaryNode<Integer> p)
	{
		int h = 0;
		for (BinaryNode<Integer> c : children(p))
		{
			h = Math.max(h, 1 + findHeight(c));
		}
		return h;
	}

	public int height()
	{
		int h = findHeight(root);
		return h;
	}

	//insert method

	public void insert(int x, BinaryNode<Integer> prevNode)
	{


		if (x - prevNode.getData() < 0)
		{
			if (left(prevNode) != null)
			{
				prevNode = left(prevNode);
				insert(x,prevNode);
			}
			
			else
			{
				addLeft(x,prevNode);
			}
		}

		else
		{
			if (right(prevNode) != null)
			{
				prevNode = right(prevNode);
				insert(x,prevNode);
			}

			else
			{
				addRight(x,prevNode);
			}
		}
	}

	//build method
	public void build(int maxHeight)
	{

		Random rand = new Random();
		int x = rand.nextInt(90) + 10;

		addRoot(x);

		while (height() < maxHeight)
		{
			x = rand.nextInt(90) + 10;
			insert(x,getRoot());
		}
	}
	

	//destroy method

	public void destroy()
	{
		BinaryNode<Integer> r = getRoot();

		if (r.nChild() == 0)
		{
			emptyRoot();
		}

		if (r.nChild() == 1)
		{
			if (r.getLeft() != null)
			{
				BinaryNode<Integer> temp = r.getLeft();
				r.setElement(temp.getData());
				r.setLeft(temp.getLeft());
				r.setRight(temp.getRight());
				temp.setParent(null);
				//setRoot(temp.getData(),null,temp.getLeft(),temp.getRight());
			}

			else
			{
				BinaryNode<Integer> temp = r.getRight();
				temp.setParent(null);
				r.setElement(temp.getData());
				r.setLeft(temp.getLeft());
				r.setRight(temp.getRight());
				//setRoot(temp.getData(),null,temp.getLeft(),temp.getRight());

			}
		}

		if (r.nChild() == 2)
		{
			if (r.getLeft().getRight() == null)
			{
				BinaryNode<Integer> temp = r.getLeft();
				r.setElement(temp.getData());
				r.setLeft(temp.getLeft());
				//setRoot(temp.getData(),null,temp.getLeft(),r.getRight());
				r.getRight().setParent(temp);
				temp.setRight(r.getRight());
				temp.setParent(null);
			}

			else if (r.getRight().getLeft() == null)
			{
				BinaryNode<Integer> temp = r.getRight();
				r.setElement(temp.getData());
				r.setRight(temp.getRight());
				//setRoot(temp.getData(),null,r.getLeft(),temp.getRight());
				r.getLeft().setParent(temp);
				temp.setLeft(r.getLeft());
				temp.setParent(null);
			}

			else
			{
				BinaryNode<Integer> targetNode = new BinaryNode<>();
				BinaryNode<Integer> recur = r.getLeft().getRight();

				//traverse
				while (recur.hasRight())
				{
					recur = recur.getRight();
				}

				targetNode = recur;
				targetNode.getParent().setRight(null);

				if (targetNode.hasLeft())
				{
					targetNode.getParent().setRight(targetNode.getLeft());
					targetNode.getLeft().setParent(targetNode.getParent());
					targetNode.setParent(null);
					targetNode.setLeft(null);
				}

				r.setElement(targetNode.getData());
				//setRoot(targetNode.getData(),null,r.getLeft(),r.getRight());
				r.getLeft().setParent(targetNode);
				r.getRight().setParent(targetNode);
				targetNode.setLeft(r.getLeft());
				targetNode.setRight(r.getRight());
				targetNode.setParent(null);
			}
		}
	}

	//AVL METHODS

	public void balance()
	{
		BinaryNode<Integer> problem = NodeCheck(getRoot());
		while (problem != null)
		{
			fixProblem(problem);
			problem = NodeCheck(getRoot());
		}

		System.out.println("NO PROBLEM DETECTED: TREE BALANCED");
	}
	public BinaryNode<Integer> NodeCheck(BinaryNode<Integer> Node) throws IllegalStateException
	{


		int leftHeight = 0;
		int rightHeight = 0;

		if (Node.hasLeft())
		{
			leftHeight = 1 + findHeight(Node.getLeft());
		}

		if (Node.hasRight())
		{
			rightHeight = 1 + findHeight(Node.getRight());
		}
		


		int z = Math.abs(leftHeight - rightHeight);
		System.out.println("CHECKING NODE: " + Node.getData() + ".... DIFFERENCE IN HEIGHT: " + z);

		if (z > 1)
		{
			System.out.println();
			System.out.println("PROBLEM DETECTED AT NODE " + Node.getData());
			
			return Node;
		}
		
		for (final BinaryNode<Integer> c : children(Node))
			{
				if (findHeight(c) > 0 && z<= 1)
				{
					final BinaryNode<Integer> res = NodeCheck(c);
					if (res != null)
					{
						return res;
					}
				}
			}
		
		

		return null;

	}

	public void fixProblem(BinaryNode<Integer> p)
	{
		int leftHeight = 0;
		int rightHeight = 0;

		if (p.hasLeft())
		{
			leftHeight = 1 + findHeight(p.getLeft());
		}

		if (p.hasRight())
		{
			rightHeight = 1 + findHeight(p.getRight());
		}

		if (leftHeight > rightHeight)
		{
			int llHeight = 0;
			int lrHeight = 0;

			if (p.getLeft().hasLeft())
			{
				llHeight = 1 + findHeight(p.getLeft().getLeft());
			}

			if (p.getLeft().hasRight())
			{
				lrHeight = 1 + findHeight(p.getLeft().getRight());
			}

			if (llHeight > lrHeight)
			{
				SingleRight(p);
			}

			else
			{
				DoubleLeftRight(p);
			}
		}

		else
		{
			int rlHeight = 0;
			int rrHeight = 0;

			if (p.getRight().hasLeft())
			{
				rlHeight = 1 + findHeight(p.getRight().getLeft());
			}

			if (p.getRight().hasRight())
			{
				rrHeight = 1 + findHeight(p.getRight().getRight());
			}

			if (rlHeight > rrHeight)
			{
				DoubleRightLeft(p);
			}

			else
			{
				SingleLeft(p);
			}
		}
	}


	public void SingleRight(BinaryNode<Integer> Node)
	{
		BinaryNode<Integer> temp = Node.getLeft();
		System.out.println("Performing SingleRight rotation on Node with value: " + Node.getData());
		if (Node.getParent() != null)
		{
			if (Node.getParent().hasLeft() && Node.getParent().getLeft().getData() == Node.getData())
			{
				if(!Node.getParent().hasParent())
					{
						root.setLeft(temp);
						temp.setParent(root);
					}
				else
					{
					Node.getParent().setLeft(temp);
					temp.setParent(Node.getParent());
					}
			}		

			else
			{
				if (Node.getParent() == root)
				{
					root.setRight(temp);
					temp.setParent(root);
				}				
				else
				{
					Node.getParent().setRight(temp);
					temp.setParent(Node.getParent());
				}
			}

		}

		else
		{
			root = temp;
		}

		if (temp.hasRight())
		{
			BinaryNode<Integer> x = temp.getRight();
			x.setParent(Node);

		}
		temp.setParent(Node.getParent());
		Node.setParent(temp);
		Node.setLeft(temp.getRight());
		//x.setParent(Node);
		temp.setRight(Node);

	} 

	
	public void SingleLeft(BinaryNode<Integer> Node)
	{
		BinaryNode<Integer> temp = Node.getRight();
		System.out.println("Performing SingleLeft rotation on Node with value: " + Node.getData());
		if (Node.hasParent())
		{
			if (Node.getParent().hasLeft() && Node.getParent().getLeft().getData() == Node.getData())
			{
				if (Node.getParent() == root)
				{
					root.setLeft(temp);
					temp.setParent(root);
				}

				else
				{
					Node.getParent().setLeft(temp);
					temp.setParent(Node.getParent());
				}
			}

			else
			{
				if (Node.getParent() == root)
				{
					root.setRight(temp);
					temp.setParent(root);
				}
				else
				{
					Node.getParent().setRight(temp);
					temp.setParent(Node.getParent());
				}
			}


		}
		
		else
		{
			root = temp;

		}
		if (temp.hasLeft())
		{
			BinaryNode<Integer> y = temp.getLeft();
			y.setParent(Node);
		}
		temp.setParent(Node.getParent());
		Node.setParent(temp);
		Node.setRight(temp.getLeft());
		//y.setParent(Node);
		temp.setLeft(Node);

	}

	public void DoubleLeftRight(BinaryNode<Integer> p)
	{
		System.out.println("Performing DoubleLeftRight rotation on node with value: " + p.getLeft().getData());

		SingleLeft(p.getLeft());
		SingleRight(p);
	}

	public void DoubleRightLeft(BinaryNode<Integer> p)
	{
		System.out.println("Performing DoubleRightLeft rotation on node with value: " + p.getRight().getData());

		SingleRight(p.getRight());
		SingleLeft(p);
	}




}