import java.util.Random;

public class Balance
{
	
	public static void main(String[] args)
	{
		
		BinarySearchTree<Integer> avl = new BinarySearchTree<>();
		
		avl.build(5);
		TreePrinter tprint = new TreePrinter(avl);
		while((avl.findHeight(avl.getRoot())) > 0 || (avl.getRoot().getData() != 0))
		{
			tprint.print("");
			BinaryNode<Integer> problem = avl.NodeCheck(avl.getRoot());
			while (problem != null)
			{
				avl.fixProblem(problem);
				tprint.print("");
				problem = avl.NodeCheck(avl.getRoot());
			}
			System.out.println("TREE RE-BALANCED..... DELETING NODE");
			avl.destroy();

		}
		System.out.println("TREE EMPTY");


		/*
		BinaryNode<Integer> t1 = avl.addRoot(26);
		BinaryNode<Integer> t2 = avl.addLeft(12,t1);
		BinaryNode<Integer> t3 = avl.addRight(13,t2);
		BinaryNode<Integer> t4 = avl.addRight(15,t3);
		BinaryNode<Integer> t5 = avl.addRight(21,t4);

		BinaryNode<Integer> t6 = avl.addRight(69,t1);
		BinaryNode<Integer> t7 = avl.addLeft(31,t6);
		BinaryNode<Integer> t8 = avl.addRight(50,t7);
		BinaryNode<Integer> t9 = avl.addLeft(43,t8);
		BinaryNode<Integer> t10 = avl.addLeft(33,t9);

		BinaryNode<Integer> t11 = avl.addRight(88,t6);
		BinaryNode<Integer> t12 = avl.addLeft(83,t11);
		BinaryNode<Integer> t13 = avl.addLeft(77,t12);
		BinaryNode<Integer> t14 = avl.addRight(91,t11);

		TreePrinter tprint = new TreePrinter(avl);

		tprint.print("");
		
		BinaryNode<Integer> problem = avl.NodeCheck(avl.getRoot());
		avl.fixProblem(problem);

		tprint.print("");

		problem = avl.NodeCheck(avl.getRoot());
		avl.fixProblem(problem);

		tprint.print("");

		problem = avl.NodeCheck(avl.getRoot());

		while (problem != null)
		{
			avl.fixProblem(problem);
			tprint.print("");
			problem = avl.NodeCheck(avl.getRoot());
		}
		*/
		/*
		avl.SingleRight(problem.getRight());

		tprint.print("");

		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();

		System.out.println("ROOT: " + avl.getRoot().getData());
		System.out.println("RIGHT CHILD: " + avl.getRoot().getRight().getData() );
		System.out.println("RIGHT CHILD PARENT: " + avl.getRoot().getRight().getParent().getData());
		System.out.println("RIGHT CHILD RIGHT CHILD: " + avl.getRoot().getRight().getRight().getData());
		System.out.println("RIGHT CHILD RIGHT CHILD PARENT: " + avl.getRoot().getRight().getRight().getParent().getData());
		System.out.println("RIGHT CHILD RIGHT CHILD LEFT CHILD: " + avl.getRoot().getRight().getRight().getLeft().getData());
		System.out.println("RIGHT CHILD RIGHT CHILD LEFT CHILD PARENT: " + avl.getRoot().getRight().getRight().getLeft().getParent().getData());
		System.out.println("Right CHILD RIGHT CHILD RIGHT CHILD: " + avl.getRoot().getRight().getRight().getRight().getData());
		System.out.println("RIGHT CHILD RIGHT CHILD RIGHT CHILD PARENT: " + avl.getRoot().getRight().getRight().getRight().getParent().getData());
		*/

		/*
		System.out.println("ROOT: " + avl.getRoot().getData());
		System.out.println("RIGHT CHILD: " + avl.getRoot().getRight().getData() );
		System.out.println("RIGHT CHILD PARENT: " + avl.getRoot().getRight().getParent().getData());
		System.out.println("RIGHT CHILD LEFT CHILD: " + avl.getRoot().getRight().getLeft().getData());
		System.out.println("RIGHT CHILD LEFT CHILD PARENT: " + avl.getRoot().getRight().getLeft().getParent().getData());

		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		*/
		/*problem = avl.NodeCheck(avl.getRoot());
		avl.fixProblem(problem);

		tprint.print("");
		problem = avl.NodeCheck(avl.getRoot());
		avl.fixProblem(problem);

		tprint.print("");*/

	}


	/*
	

	public void SingleRight(BinaryNode<Integer> Node)
	{
		BinaryNode<Integer> temp = Node.getLeft();
		System.out.println("Performing SingleRight rotation on Node with value: " + temp.getData());
		if (Node.hasParent())
		{
			if (Node.getParent().getLeft().getData() == Node.getData())
			{
				Node.getParent().setLeft(temp);
			}

			else
			{
				Node.getParent().setRight(temp);
			}

			temp.setParent(Node.getParent());
		}

		else
		{
			root = temp;
		}

		temp.setParent(Node.getParent());
		Node.setParent(temp);
		Node.setLeft(temp.getRight());
		temp.setRight(Node);

	} */

	/*
	public void SingleLeft(BinaryNode<Integer> Node)
	{
		BinaryNode<Integer> temp = Node.getRight();
		System.out.println("Performing SingleLeft rotation on Node with value: " + temp.getData());
		if (Node.hasParent())
		{
			if (Node.getParent().getLeft().getData() == Node.getData())
			{
				Node.getParent().setLeft(temp);
			}

			else
			{
				Node.getParent().setRight(temp);
			}

			temp.setParent(Node.getParent());
		}
		
		else
		{
			root = temp;
		}
			
		Node.setParent(temp);
		Node.setRight(temp.getLeft());
		temp.setLeft(Node);



	} */


	/*public void DoubleLeftRIght(BinaryNode<Integer> Node)
	{
		System.out.println("Performing DoubleLeftRight rotation on Node with value: " + Node.getData());

		BinaryNode<Integer> temp = Node.getLeft();
	}
	*/

	/*public void SingleLeft(BinaryNode<Integer> Node)
	{
		BinaryNode<Integer> temp = Node.getRight();
		System.out.println("Performing SingleLeft rotation on Node with value: " + temp.getData());
		if (Node.hasParent())
		{
			if (Node.getParent().getLeft().getData() == Node.getData())
			{
				Node.getParent().setLeft(temp);
			}

			else
			{
				Node.getParent().setRight(temp);
			}
		}

		temp.setParent(Node.getParent());
		Node.setParent(temp);
		Node.setRight(temp.getLeft());
		temp.setLeft(Node);



	}*/

}

