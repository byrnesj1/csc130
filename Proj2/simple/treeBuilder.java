import java.util.Random;

public class treeBuilder
{
	
	public static void main(String[] args)
	{
		
		//build method
		BinarySearchTree<Integer> bst = new BinarySearchTree<>();			
		Random rand = new Random();
		int x = rand.nextInt(90) + 10;
	
		bst.addRoot(x);
	
		while (bst.height() < 5)
		{
			x = rand.nextInt(90) + 10;
			bst.insert(x,bst.getRoot());
	
		}

		TreePrinter tprint = new TreePrinter(bst);
		tprint.print("");	
	
		//destroy method
		while (bst.height() > 0)
		{
			BinaryNode<Integer> r = bst.getRoot();

			if (r.nChild() == 0)
			{
				bst.emptyRoot();
			}

			if (r.nChild() == 1)
			{
				if (r.getLeft() != null)
				{
					//switch root with root.getLeft()
					BinaryNode<Integer> temp = r.getLeft();
					bst.setRoot(temp.getData(),null,temp.getLeft(),temp.getRight());
				}

				else
				{
					BinaryNode<Integer> temp = r.getRight();
					bst.setRoot(temp.getData(),null,temp.getLeft(),temp.getRight());
				}
			}

			if (r.nChild() == 2)
			{
				if (r.getLeft().getRight() == null)
				{
					//bst.switchRoot(r, r.getLeft());
					BinaryNode<Integer> temp = r.getLeft();
					bst.setRoot(temp.getData(),null,temp.getLeft(),r.getRight());
				}

				else if(r.getRight().getLeft() == null)
				{
					//bst.switchRoot(r,r.getRight());
					BinaryNode<Integer> temp = r.getRight();
					bst.setRoot(temp.getData(),null, r.getLeft(), temp.getRight());
				}
				else
				{
					
					BinaryNode<Integer> targetNode = new BinaryNode<>();
					BinaryNode<Integer> recur = r.getLeft().getRight();
					
					//traverse
					while (recur.hasRight())
					{
						recur = recur.getRight();
					}

					targetNode = recur;
					targetNode.getParent().setRight(null);


					if (targetNode.hasLeft())
					{
						targetNode.getParent().setRight(targetNode.getLeft());
						targetNode.getLeft().setParent(targetNode.getParent());
						targetNode.setParent(null);
						targetNode.setLeft(null);
					}
					bst.setRoot(targetNode.getData(),null, r.getLeft(), r.getRight());

				
				}
			}

			tprint.print("");		



		}
	}
}