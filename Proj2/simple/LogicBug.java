public class LogicBug
{
	public static void main(String[] args)
	{
		BinarySearchTree<Integer> avl = new BinarySearchTree<>();

		BinaryNode<Integer> t1 = avl.addRoot(95);
		BinaryNode<Integer> t2 = avl.addRight(18,t1);
		BinaryNode<Integer> t3 = avl.addLeft(59,t2);

		BinaryNode<Integer> problem = avl.NodeCheck(avl.getRoot());
		TreePrinter tprint = new TreePrinter(avl);
		tprint.print("");

		avl.fixProblem(problem);
		tprint.print("");
	}
}