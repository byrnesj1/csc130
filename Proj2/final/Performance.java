import java.util.Random;

public class Performance
{
	int size;
	public static void main(String[] args)
	{
		Performance trial = new Performance(750);

		
		int datas[] = new int[trial.size];
		Random rand = new Random();
		for (int n = 0; n < trial.size; n++)
		{
			int x = rand.nextInt(90) + 10;
			datas[n] = x;
		}

		long startTime = System.currentTimeMillis();
		trial.bstPerf(datas);
		long endTime = System.currentTimeMillis();

		
		long startTime2 = System.currentTimeMillis();
		trial.avlPerf(datas);
		long endTime2 = System.currentTimeMillis();
		
		System.out.println("The execution of building a BST with " + trial.size + " elements took: " + (endTime - startTime) + " milliseconds.");
		System.out.println("The execution of building an AVL with " + trial.size + " elements took: " + (endTime2 - startTime2) + " milliseconds.");

	}

	public Performance(int size)
	{
		this.size = size;
	}
 	
	public void bstPerf(int[] datas)
	{
		BinarySearchTree<Integer> bst = new BinarySearchTree<>();
		bst.addRoot(datas[0]);
		for (int i = 1; i < datas.length; i++)
		{				
			bst.insert(datas[i],bst.getRoot());
		}
	}	

	public void avlPerf(int[] datas)
	{
		BinarySearchTree<Integer> avl = new BinarySearchTree<>();

		avl.addRoot(datas[0]);

		for (int i = 1; i < datas.length; i++)
		{
			avl.insert(datas[i],avl.getRoot());

		}

		avl.balance();
	}
}