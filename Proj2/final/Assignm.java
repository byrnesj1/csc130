import java.util.Random;

public class Assignm

{
	public static void main(String[] args)
	{
		
			BinarySearchTree<Integer> bst = new BinarySearchTree<>();		
			//Generate and print random BST with height 5		
			System.out.println("Building random BST with height 5");
			System.out.println();
			System.out.println();
			System.out.println();		
			
			bst.build(5);
			TreePrinter tprint = new TreePrinter(bst);				
			tprint.print("");		
			
			//Repetitively delete root and print tree		
			System.out.println("Repetitively deleting root");
			System.out.println();
			System.out.println();
			System.out.println();
			while ((bst.findHeight(bst.getRoot()) > 0) && (bst.getRoot().getData() != 0))
			{
				System.out.println("Deleting root");
				bst.destroy();
				tprint.print("");
			} 
			System.out.println("Tree Empty");


	}
}