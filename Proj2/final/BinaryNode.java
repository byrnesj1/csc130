public class BinaryNode<Integer> implements BNode
{
	private int element; //
	private BinaryNode<Integer> parent;
	private BinaryNode<Integer> left;
	private BinaryNode<Integer> right;



	;



	//
	
	public BinaryNode()
	{
		int element;
		BinaryNode<Integer> parent;
		BinaryNode<Integer> left;
		BinaryNode<Integer> right;

	}

	public BinaryNode(int element, BinaryNode<Integer> parent, BinaryNode<Integer> left, BinaryNode<Integer> right)
	{
		this.element = element;
		this.parent = parent;
		this.left = left;
		this.right = right;
	}

	//get methods
	public int getData() {return element;} //
	public BinaryNode<Integer> getParent() {return parent;}
	public BinaryNode<Integer> getLeft() {return left;}
	public BinaryNode<Integer> getRight() {return right;}

	//set methods
	public void setElement(int e) {element = e;} //
	public void setParent(BinaryNode<Integer> p) {parent = p;}
	public void setLeft(BinaryNode<Integer> l) {left = l;}
	public void setRight(BinaryNode<Integer> r) {right = r;}

	//has methods

	public boolean hasLeft()
	{
		boolean out = false;
		if (left != null) {out = true;}
		return out;
	}

	public boolean hasRight()
	{
		boolean out = false;
		if (right != null) {out = true;}
		return out;
	}

	public boolean hasParent()
	{
		boolean out = false;
		if (parent != null) {out = true;}
		return out;
		
	}


	//children
	public int nChild()
	{
		int nChild =0;

		if (right != null) {++nChild;}
		if (left != null) {++nChild;}

		return nChild;
	}

	public int depth()
	{
		int depth = 0;
		{
			while(hasParent())
			{
				++depth;
			}
		return depth;
		}
	}
}