import java.util.Random;

public class Assignm2
{
	public static void main(String[] args)
	{

		//create AVL tree with 33 random elements

			System.out.println("Building AVL Tree with 35 random elements");
			System.out.println();
			System.out.println();
			System.out.println();
			
			BinarySearchTree<Integer> avl = new BinarySearchTree<>();
			Random rand = new Random();
			int x = rand.nextInt(90) + 10;
			avl.addRoot(x);
			TreePrinter tprints = new TreePrinter(avl);
			tprints.print("");		

			for (int i = 0; i < 34; i++)
			{		
				x = rand.nextInt(90) + 10;
				avl.insert(x,avl.getRoot());
				avl.balance();
				tprints = new TreePrinter(avl);
				tprints.print("");
				System.out.println();
			}


			System.out.println("Repetitevly deleting root Node until empty");
			System.out.println();
			System.out.println();
			System.out.println();

			while ((avl.findHeight(avl.getRoot()) > 0) && (avl.getRoot().getData() != 0))
			{
				System.out.println("Deleting root");
				avl.destroy();
				tprints.print("");
				System.out.println("Balancing Tree");
				avl.balance();
				tprints.print("");
			} 
			System.out.println("Tree Empty");


	}
}