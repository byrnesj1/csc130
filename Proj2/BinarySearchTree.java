public class BinarySearchTree<Integer> extends AbstractBinaryTree<Integer>
{
	//BinaryNodeC subclass
	protected static class BinaryNodeC<Integer> implements BinaryNode<Integer>
	{
		//instance variables
		private Integer element;
		private BinaryNodeC<Integer> parent;
		private BinaryNodeC<Integer> left;
		private BinaryNodeC<Integer> right;

		//constructor
		public BinaryNodeC(Integer element, BinaryNodeC<Integer> parent, BinaryNodeC<Integer> left, BinaryNodeC<Integer> right)
		{
			this.element = element;
			this.parent = parent;
			this.left = left;
			this.right = right;
		}

		//get methods
		public Integer getElement() {return element;}
		public BinaryNodeC<Integer> getParent() {return parent;}
		public BinaryNodeC<Integer> getLeft() {return left;}
		public BinaryNodeC<Integer> getRight() {return right;}

		//set methods
		public void setElement(Integer element) {this.element = element;}
		public void setParent(BinaryNodeC<Integer> parent) {this.parent = parent;}
		public void setLeft(BinaryNodeC<Integer> left) {this.left = left;}
		public void setRight(BinaryNodeC<Integer> right) {this.right = right;}
	}


	//create node method for BST
	protected BinaryNodeC<Integer> createNode(Integer element, BinaryNodeC<Integer> parent, BinaryNodeC<Integer> left, BinaryNodeC<Integer> right)
	{
		return new BinaryNodeC<Integer>(element,parent,left,right);
	}

	//instance variables
	protected BinaryNodeC<Integer> root = null;
	private int size = 0;

	//null constructor
	public BinarySearchTree(){ }

	//accessor methods
	public int size()
	{
		return size;
	}

	public BinaryNodeC<Integer> root()
	{
		return root;
	}

	public BinaryNodeC<Integer> parent(BinaryNodeC<Integer> p)
	{
		return p.getParent();
	}

	public BinaryNodeC<Integer> left(BinaryNodeC<Integer> p)
	{
		return p.getLeft();
	}

	public BinaryNodeC<Integer> right(BinaryNodeC<Integer> p)
	{
		return p.getRight();
	}


	//update methods

	//root
	public BinaryNodeC<Integer> addRoot(Integer e) throws IllegalStateException
	{
		if (!isEmpty()) throw new IllegalStateException("Tree is not empty");
		root = createNode(e,null,null,null);
		size = 1;
		return root;
	}

	//left child
	public BinaryNodeC<Integer> addLeft(BinaryNodeC<Integer> p, Integer e) throws IllegalArgumentException
	{
		if (p.getLeft() != null)
		{
			throw new IllegalArgumentException("P already has left child");
		}
		BinaryNodeC<Integer> child = createNode(e,p,null,null);
		p.setLeft(child);
		size++;
		return child;
	}

	public BinaryNodeC<Integer> addRight(BinaryNodeC<Integer> p, Integer e) throws IllegalArgumentException
	{
		if (p.getRight() != null)
		{
			throw new IllegalArgumentException("P already has a right child");
		}

		BinaryNodeC<Integer> child = createNode(e,p,null,null);
		p.setRight(child);
		size++;
		return child;
	}

	//update current node (Integer) with user defined

	public Integer set(BinaryNodeC<Integer> node, Integer e)
	{
		Integer temp = node.getElement();
		node.setElement(e);
		return temp;

	}


	
}